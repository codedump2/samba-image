# samba-image

This defines a Docker image for a samba file server. 

## Configuration

The main configuration file is held simple and permissive,
essentially parallel to:
```
[global]
        include = /var/samba/conf/global.conf
         
[homes]
        include = /var/samba/conf/homes.conf

[printers]
        include = /var/samba/conf/printers.conf


include = /var/samba/conf/shares.conf

```

The directory `/var/samba/conf` should be mounted from the host,
but there is a populated template inside the image that serves
a shared named `[public]` with guest-enabled, read-only,
browseable options from `/var/samba/public`. By default that 
directory is empty, though.

## Running

Out of the box, it serves the directory `/var/samba/public` as
a read-only share, so starting e.g. like this should automagically
serve the `$DIR` directory:
```
    podman run -v $DIR:/var/samba/public samba-image
```

**BEWARE** that this will expose data in `$DIR` and all its
subdirectories to anyone who cares to connect!

The directory `/var/samba/conf` is expected to contain the following
files for fine-grained configuration adjustments:
  - `conf/global.conf`: for keys in the smb.conf `[globals]` section
  - `conf/homes.conf`: for keys in the `[homes]` section
  - `conf/printers.conf`: for keys in the `[printers]` section
  - `conf/shares.conf`: for definition (section *and* keys) of more shares
  
So the favorite way of running it would be:
```
    podman run \
	    -v $DATA:/var/samba/public \
		-v $CONF:/var/samba/conf \
		... \
		-p 137-139/tcp
		samba-image
```
where `$DATA` is a read-only directory, and `$CONF` is a directory
containing customized files.

## Authentication

If no authentication server (e.g. LDAP) is used, it is suggested that
the authentication data is kept under `$AUTH` on the host and exposed
to the container via:
```
    podman run ... -v $AUTH:/var/samba/private
```
and the following line is added to `global.conf`:
```
   ...
   passdb backend = tdbsam:/var/samba/private/passdb.tdb
   ...
```
