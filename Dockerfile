FROM debian:10
RUN     apt-get update && apt-get install -y \
	nano samba smbclient && \
	apt-get autoclean && \
	apt-get autoremove && \
	mkdir -p /var/samba/public && \
	mkdir -p /var/samba/conf && \
	mkdir -p /var/samba/private

COPY ./conf/smb.conf /etc/samba/smb.conf
COPY ./conf/*conf    /var/samba/conf/

CMD smbd -S -F

