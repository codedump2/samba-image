#!/bin/bash

sudo podman run -ti --rm \
     -v /var/home/florin/tmp/tmp:/var/samba/public:z \
     -v /var/home/florin/projects/samba-image/conf:/var/samba/conf:z \
     -p 137-139:137-139/tcp -p 445:445/tcp \
     registry.gitlab.com/codedump2/samba-image
